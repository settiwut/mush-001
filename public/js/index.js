'use strict'

function signIn() {

    var email = document.getElementById("email-signin").value;
    var password = document.getElementById("password-signin").value;
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function () {
            document.location.href = "home.html";
        })
        .catch(function (error) {
            var errorMessage = error.message;
            window.alert("Error : " + errorMessage);
            console.log(error);
        });
}

// function signup() {

//     var email = document.getElementById('email-signup').value;
//     var password = document.getElementById('password-signup').value;
//     var username = document.getElementById('username-signup').value;
//     var name = document.getElementById('name-signup').value;
//     var surname = document.getElementById('surname-signup').value;
//     var joposition = document.getElementById('joposition-signup').value;

//     console.log(email,
//         password,
//         username,
//         name,
//         surname,
//         joposition);

//     firebase.auth().signInWithEmailAndPassword(email, password)
//         .catch(function (error) {
//             try {
//                 var firebaseRef = firebase.database().ref("users");
//                 firebaseRef.push({
//                     email: email,
//                     password: password,
//                     name: name,
//                     username: username,
//                     surname: surname,
//                     joposition: joposition
//                 })
//                 console.log("Insert Success");
//             } catch (error) {
//                 var errorCode = error.code;
//                 var errorMessage = error.message;
//                 if (errorCode === 'auth/wrong-password') {
//                     window.alert('Wrong password.');
//                 } else {
//                     window.alert(errorMessage);
//                 }
//             }
//         });
// }

function logout() {
    firebase.auth().signOut().then(function () {
        document.location.href = "index.html";
    });
}


function getEvents() {

    // var demo2 = document.getElementById('demo2');
    // var dbRef = firebase.database().ref("user").on('value', snapshot => demo2.innerText = snapshot.val());
}

function pushEvents() {

}

function initHome() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            console.log('user', user)
            document.getElementById("user_para").innerHTML = "Welcome User : " + email;
        }
        else {
            console.log('555555')
            document.location = "index.html";
        }
    })
}

function initApp() {
    // // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
    // // The Firebase SDK is initialized and available here!
    // firebase.auth().onAuthStateChanged(user => { });
    // firebase.database().ref('/path/to/ref').on('value', snapshot => { });

    firebase.auth().onAuthStateChanged(function (user) {
        //console.log(user)
        if (user) {
            // User is signed in.
            // document.getElementById("user_div").style.display = "block"; // ปิด
            //document.getElementById("login_div").style.display = "none"; // เปิด
            //var user = firebase.auth().currentUser;

            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;
            //document.location.href = "home.html";
            var user_para = document.getElementById("user_para")
            if (user_para) user_para.innerHTML = email;
        } else {
            //document.location.href = "index.html";
            // No user is signed in.
            // document.getElementById("user_div").style.display = "none";
            //document.getElementById("login_div").style.display = "block";
            //if(window.location.href)document.location = "index.html";
            //console.log(window.location.href)
            console.log(getCookie("path"));
            if (getCookie("path")) {
                if (getCookie("path").includes("home")) document.location = "index.html";
            }
        }
    });
    // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

window.onload = function () {
    var firebaseConfig = {
        apiKey: "AIzaSyBJbYRt-riDmws4FgmPyynsiOP5mVQBpDI",
        authDomain: "mush-6cb13.firebaseapp.com",
        databaseURL: "https://mush-6cb13.firebaseio.com",
        projectId: "mush-6cb13",
        storageBucket: "mush-6cb13.appspot.com",
        messagingSenderId: "750937800479",
        appId: "1:750937800479:web:73af3fe7b87442b9fcfa2a"
    };
    // Initialize Firebase
    document.cookie = "path=" + window.location.href + ";";
    if (!firebase.apps.length) {
        console.log("firebase.apps.length")
        firebase.initializeApp(firebaseConfig);
        initApp();
    }

};