function signup() {
    console.log('loading');

    var email = document.getElementById('email-signup').value;
    var password = document.getElementById('password-signup').value;
    var username = document.getElementById('username-signup').value;
    var name = document.getElementById('name-signup').value;
    var surname = document.getElementById('surname-signup').value;
    var joposition = document.getElementById('joposition-signup').value;

    if (!email || !password || !username || !name || !surname || !joposition) {
        // alert("กรุณากรอกข้อมูลให้ครบ")
        swal({
            title: "เกิดข้อผิดพลาด?",
            text: "กรุณากรอกข้อมูลให้ครบ",
            icon: "warning",
        })
    } else {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(res => {
                var firebaseRef = firebase.database().ref(`users/${res.user.uid}`);
                firebaseRef.set({
                    email: email,
                    password: password,
                    name: name,
                    username: username,
                    surname: surname,
                    joposition: joposition,
                    uid: res.user.uid
                })
                    .then(() => {
                        console.log("Create Account Success");
                        console.log('finish');
                        document.location = "home.html";
                        //swal("Good job!", "Create Account Success!", "success");
                    })
                    .catch(({ message }) => {
                        console.log('finish');
                        swal({
                            title: "เกิดข้อผิดพลาด?",
                            text: message,
                            icon: "warning",
                        })
                    })
            })
            .catch(({ message }) => {
                console.log('finish');
                swal({
                    title: "เกิดข้อผิดพลาด?",
                    text: message,
                    icon: "warning",
                })
            })
        
    }



}
